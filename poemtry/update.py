from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import pickle
from time import sleep
import logging
import threading

# TODO: Adapt notebook update with threading into this file.
BASE_URL = 'https://poetryfoundation.org'


def set_drivers(num: int):
    """
    Creates a number of drivers to use in threading

    :param num: int -- number of drivers
    :return: tuple of drivers
    """
    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--incognito')  # this is to ping I want it to gooooooo
    options.add_argument('--headless')

    # TODO: Find out path for macOS and RPI
    driver_path = '/usr/bin/chromewebdriver'
    drivers = (webdriver.Chrome(driver_path, options=options) for i
               in range(num))

    return drivers


def update():
    drivers = set_drivers(1)
    pass
