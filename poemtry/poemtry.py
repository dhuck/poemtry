from bs4 import BeautifulSoup as bs
import requests
import random
import logging
from .loader import load_poems, reset_history
from requests.exceptions import ConnectionError


class ScreenError(Exception):
    """Raised to pass the error up to the main caller"""
    pass

try:
    from PIL import Image, ImageFont, ImageDraw
    from inky import InkyWHAT
    from font_roboto import Roboto, RobotoBold


    class Screen(InkyWHAT):
        def __init__(self, colour):
            super().__init__(colour)
            logging.debug("Initialized the Object")
            self.__base_url = 'https://www.poetryfoundation.org/'
            self.__padding: int = 30
            self.__max_width = self.WIDTH - self.__padding
            self.__title_font_size: int = 20
            self.__author_font_size: int = 14
            self.__body_font_size: int = 14

            self.__title_font = ImageFont.truetype(RobotoBold,
                                                   self.__title_font_size)
            self.__author_font = ImageFont.truetype(Roboto,
                                                    self.__author_font_size)
            self.__body_font = ImageFont.truetype(Roboto,
                                                  self.__body_font_size)

            try:
                self.__poems, self.__hist = load_poems()
            except FileNotFoundError:
                msg = "File not found! Please run update script to generate " \
                      "pickle files!"
                raise ScreenError(msg)

            except EOFError:
                msg = "Pickle files corrupted! Run update script to generate " \
                      "new pickle files!"
                raise ScreenError(msg)

            self.__html = ''
            self.__title: str = ''
            self.__author: str = ''
            self.__body: list = list()

        def get_poem(self):
            logging.debug("Choosing url from list")
            url = random.choice(self.__poems)
            not_new = True
            while not_new:
                if url not in self.__hist:
                    not_new = False
            self.__hist.append(url)
            url = self.__base_url + url
            logging.debug(f'Retreiving {url}')
            source = requests.get(url).text
            self.__html = bs(source, 'html.parser')

        def get_header(self):
            title = self.__html.find('h1').get_text().strip()
            self.__title = self.parse_line(title, self.__title_font)

            attribution = self.__html.find('div', {'class': 'c-feature-sub'})
            people = attribution.find_all('span')

            if len(people) == 2:
                translator = f'(tr {people[1].get_text().strip()[14:]})'
            else:
                translator = ''

            author = people[0].get_text().strip()[3:] + translator
            self.__author = self.parse_line(author, self.__author_font)

        def get_body(self):
            body = self.__html.find('div', {'class': 'o-poem'})
            lines = body.find_all('div',
                                  {'style': 'text-indent: -1em; '
                                            'padding-left: 1em;'})

            self.__body = '\n'.join([line.get_text().strip('\r ')
                                     for line in lines])

            # TODO: Paginate the body!
            # TODO: do a try / except for an empty body

        def parse_line(self, line, font, tab=False):
            """
            Formats a long line to one that will fit on the provided display
            area

            :param self:
            :param tab: bool. Set true to add a tab  character for new line in
            body
            :param line: String
            :param font: ImageFont object
            :return: string
            """
            words = line.split()
            line = 0
            output = ''
            for word in words:
                word_ = word + " "
                word_length = font.getsize(word_)[0]
                line += word_length

                if line < self.__max_width:
                    output += word_
                else:
                    line = word_length
                    if tab:
                        output = output[:-1] + '\n' + (' ' * 4) + word_
                    else:
                        output = output[:-1] + '\n' + word_

            return output

        def display_poem_screen(self):
            img = Image.new("L", (self.WIDTH, self.HEIGHT))
            draw = ImageDraw.Draw(img)

            title_height = self.__title_font.getsize_multiline(self.__title)[1]
            author_height = self.__author_font.getsize_multiline(
                self.__author)[1]

            left_margin = self.__padding / 2
            title_start = self.__padding / 2
            author_start = title_start + title_height + 4
            body_start = (self.__body_font_size * 2) + title_height + \
                         author_height + 4

            draw.multiline_text((left_margin, title_start), self.__title,
                                fill=self.BLACK, font=self.__title_font)
            draw.multiline_text((left_margin, author_start), self.__author,
                                fill=self.BLACK, font=self.__author_font)
            draw.multiline_text((left_margin, body_start), self.__body,
                                fill=self.BLACK, font=self.__body_font)
            self.set_image(img)
            self.show()

        def make_display(self):
            try:
                logging.info("Picking a poem")
                self.get_poem()

                logging.info('Parsing the Header')
                self.get_header()

                logging.info('Parsing the body')
                self.get_body()

                logging.info(
                    f'Displaying poem:\n\n{self.__title}\n{self.__author}\n\n'
                    f'{self.__body}')
                self.display_poem_screen()
            except ConnectionError:
                msg = 'Unable to connect to server! Check connection and try ' \
                      'again!'
                raise ScreenError(msg)

        def error_screen(self, msg):
            img = Image.new("L", (self.WIDTH, self.HEIGHT))
            draw = ImageDraw.Draw(img)
            msg = self.parse_line(msg, self.__title_font, tab=True)
            left_margin = self.__padding / 2
            draw.multiline_text((left_margin, left_margin), msg,
                                fill=self.BLACK,
                                font=self.__title_font)

            self.set_image(img)
            self.show()

        def clean(self):
            # TODO: Implement cleaning code
            pass
except ModuleNotFoundError:
    logging.warning("Critical modules not found! Please install PIL, inky, and "
                    "font_roboto modules and try again.")

if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',
                        level=logging.DEBUG)
    da_screen = Screen('black')
    da_screen.make_display()
else:
    pass
