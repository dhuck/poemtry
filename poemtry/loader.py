import logging
import pickle

POEMS = './data/text_poems_urls.pkl'
HISTORY = './data/history.pkl'


def load_poems():
    """
    Loads the pickle files that hold the history and poem URL files

    :return: list of poem urls and previously displayed poem urls
    """
    poems = pickle.load(open(POEMS, 'rb'))
    # TODO: Make a proper history.pkl
    hist = pickle.load(open(HISTORY, 'rb'))
    return poems, hist


def reset_history():
    """
    Resets the history file.

    :return: none
    """
    history = []
