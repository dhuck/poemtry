#! /usr/bin/python3

# import basic python libs
import logging
import argparse
from time import sleep  # needed for auto scroll
import poemtry as p
from poemtry import ScreenError


def setup():
    pass


# set up argpase
parser = argparse.ArgumentParser()
parser.add_argument('-u', '--update', action='store_true',
                    help='Runs a script to update the pickle library')
parser.add_argument('-r', '--reset', action='store_true',
                    help='reset the history file')
parser.add_argument('-l', '--loop', action='store', default=0,
                    help='Display in a loop of x seconds')
parser.add_argument('-c', '--clean', action='store_true',
                    help='cleans the display')
args = parser.parse_args()

# set up logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',
                    level=logging.INFO)


def main():
    logging.info("Started Poetry Parser!")
    logging.info("Loading poems and display history")
    try:
        screen = p.Screen('black')
        if args.loop != 0:
            while True:
                try:
                    screen.make_display()
                    sleep(args.continuous)
                except ScreenError as e:
                    screen.error_screen(e.args[0])
                    logging.critical(e)
                    return 1
                except KeyboardInterrupt:
                    logging.info("Exiting...")
                    return 0
        elif args.clean:
            screen.clean()
        elif args.update:
            logging.info("Updating pickle files")
            p.update()
        elif args.reset:
            p.reset_history()
        else:
            logging.warning("Button not yet implemented! Displaying poem and "
                            "exiting")
            try:
                screen.make_display()
            except ScreenError as e:
                screen.error_screen(e.args[0])
                logging.critical(e)
            logging.info("Exiting...")
            return 1
    except ScreenError as e:
        logging.critical(e)
        return 1
    except AttributeError:
        logging.warning("Screen not found, please run again on a Raspberry Pi "
                        "or run with the -u flag to generate the pkl file")
        return 1


if __name__ == '__main__':
    setup()
    main()
else:
    pass
