# Poemtry

This is simple python script to be used with the inky wHAT e-ink screen. It 
fetches a random poem from  [poetryfoundation](https://poetryfoundation.org) 
and displays it on the inky screen. Requires a Raspberry Pi and inky wHAT screen 
to function. Text poem URLs can be found in `text_poems_urls.pkl` for use in 
similar projects.